<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateControlePagamentosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('controle_pagamentos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_fluxo_saidas')->unsigned();
            $table->dateTime('vencimento');
            $table->string('situacao');
            $table->decimal('vlr_pago', 24,2)->default(0);
            $table->decimal('vlr_saldo', 24,2)->default(0);
            $table->string('promissoria');
            $table->string('parcela');
            $table->string('status');
            $table->timestamps();
        });
        Schema::table('controle_pagamentos', function($table) {
            $table->foreign('id_fluxo_saidas')->references('id')->on('fluxo_saidas')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('promissorias');
    }
}
