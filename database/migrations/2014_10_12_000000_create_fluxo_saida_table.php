<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFluxoSaidaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fluxo_saidas', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_cliente')->unsigned();
            $table->decimal('valor', 24,2)->default(0);
            $table->integer('parcelas');
            $table->dateTime('vencimento');
            $table->integer('juros');
            $table->decimal('valor_parcela', 24,2)->default(0);
            $table->decimal('valor_total', 24,2)->default(0);
            $table->string('status');
            $table->timestamps();
        });
        Schema::table('fluxo_saidas', function($table) {
            $table->foreign('id_cliente')->references('id')->on('clientes')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fluxo_saidas');
    }
}
