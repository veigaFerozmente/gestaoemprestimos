<?php return array (
  'sans-serif' => array(
    'normal' => $rootDir . '/lib/fonts/Helvetica',
    'bold' => $rootDir . '/lib/fonts/Helvetica-Bold',
    'italic' => $rootDir . '/lib/fonts/Helvetica-Oblique',
    'bold_italic' => $rootDir . '/lib/fonts/Helvetica-BoldOblique',
  ),
  'times' => array(
    'normal' => $rootDir . '/lib/fonts/Times-Roman',
    'bold' => $rootDir . '/lib/fonts/Times-Bold',
    'italic' => $rootDir . '/lib/fonts/Times-Italic',
    'bold_italic' => $rootDir . '/lib/fonts/Times-BoldItalic',
  ),
  'times-roman' => array(
    'normal' => $rootDir . '/lib/fonts/Times-Roman',
    'bold' => $rootDir . '/lib/fonts/Times-Bold',
    'italic' => $rootDir . '/lib/fonts/Times-Italic',
    'bold_italic' => $rootDir . '/lib/fonts/Times-BoldItalic',
  ),
  'courier' => array(
    'normal' => $rootDir . '/lib/fonts/Courier',
    'bold' => $rootDir . '/lib/fonts/Courier-Bold',
    'italic' => $rootDir . '/lib/fonts/Courier-Oblique',
    'bold_italic' => $rootDir . '/lib/fonts/Courier-BoldOblique',
  ),
  'helvetica' => array(
    'normal' => $rootDir . '/lib/fonts/Helvetica',
    'bold' => $rootDir . '/lib/fonts/Helvetica-Bold',
    'italic' => $rootDir . '/lib/fonts/Helvetica-Oblique',
    'bold_italic' => $rootDir . '/lib/fonts/Helvetica-BoldOblique',
  ),
  'zapfdingbats' => array(
    'normal' => $rootDir . '/lib/fonts/ZapfDingbats',
    'bold' => $rootDir . '/lib/fonts/ZapfDingbats',
    'italic' => $rootDir . '/lib/fonts/ZapfDingbats',
    'bold_italic' => $rootDir . '/lib/fonts/ZapfDingbats',
  ),
  'symbol' => array(
    'normal' => $rootDir . '/lib/fonts/Symbol',
    'bold' => $rootDir . '/lib/fonts/Symbol',
    'italic' => $rootDir . '/lib/fonts/Symbol',
    'bold_italic' => $rootDir . '/lib/fonts/Symbol',
  ),
  'serif' => array(
    'normal' => $rootDir . '/lib/fonts/Times-Roman',
    'bold' => $rootDir . '/lib/fonts/Times-Bold',
    'italic' => $rootDir . '/lib/fonts/Times-Italic',
    'bold_italic' => $rootDir . '/lib/fonts/Times-BoldItalic',
  ),
  'monospace' => array(
    'normal' => $rootDir . '/lib/fonts/Courier',
    'bold' => $rootDir . '/lib/fonts/Courier-Bold',
    'italic' => $rootDir . '/lib/fonts/Courier-Oblique',
    'bold_italic' => $rootDir . '/lib/fonts/Courier-BoldOblique',
  ),
  'fixed' => array(
    'normal' => $rootDir . '/lib/fonts/Courier',
    'bold' => $rootDir . '/lib/fonts/Courier-Bold',
    'italic' => $rootDir . '/lib/fonts/Courier-Oblique',
    'bold_italic' => $rootDir . '/lib/fonts/Courier-BoldOblique',
  ),
  'dejavu sans' => array(
    'bold' => $rootDir . '/lib/fonts/DejaVuSans-Bold',
    'bold_italic' => $rootDir . '/lib/fonts/DejaVuSans-BoldOblique',
    'italic' => $rootDir . '/lib/fonts/DejaVuSans-Oblique',
    'normal' => $rootDir . '/lib/fonts/DejaVuSans',
  ),
  'dejavu sans mono' => array(
    'bold' => $rootDir . '/lib/fonts/DejaVuSansMono-Bold',
    'bold_italic' => $rootDir . '/lib/fonts/DejaVuSansMono-BoldOblique',
    'italic' => $rootDir . '/lib/fonts/DejaVuSansMono-Oblique',
    'normal' => $rootDir . '/lib/fonts/DejaVuSansMono',
  ),
  'dejavu serif' => array(
    'bold' => $rootDir . '/lib/fonts/DejaVuSerif-Bold',
    'bold_italic' => $rootDir . '/lib/fonts/DejaVuSerif-BoldItalic',
    'italic' => $rootDir . '/lib/fonts/DejaVuSerif-Italic',
    'normal' => $rootDir . '/lib/fonts/DejaVuSerif',
  ),
  'poppins' => array(
    '200' => $fontDir . '/poppins-200_fded72f0f2a424c45d7e69525941af9a',
    '300' => $fontDir . '/poppins-300_2f2190b7aa8b6f42562cefe285b3377b',
    'normal' => $fontDir . '/poppins-normal_664fe5f9bfd448634c82003d4b8f8673',
    '600' => $fontDir . '/poppins-600_9ce4af3b3b27f6359864bcc267d44598',
    'bold' => $fontDir . '/poppins-bold_2ebb576bc53ad21da6fd75a53a6b3408',
    '800' => $fontDir . '/poppins-800_8a415ce4cd3ed732ea7cd8104614a632',
  ),
  'font awesome 5 brands' => array(
    'normal' => $fontDir . '/font-awesome-5-brands-normal_fd91aa5e3373ef491ab1f7d3d1103892',
  ),
  'font awesome 5 free' => array(
    'normal' => $fontDir . '/font-awesome-5-free-normal_47c5b2d00d8b1faca2dfa5330db572f6',
    '900' => $fontDir . '/font-awesome-5-free-900_716a505c720f3076ba191932863d46ac',
  ),
  'fontawesome' => array(
    'normal' => $fontDir . '/fontawesome-normal_43ed29441a44386a5b3b93c46e9b6448',
  ),
  'font awesome\\ 5 brands' => array(
    'normal' => $fontDir . '/font-awesome-5-brands-normal_fd91aa5e3373ef491ab1f7d3d1103892',
  ),
  'font awesome\\ 5 free' => array(
    'normal' => $fontDir . '/font-awesome-5-free-normal_47c5b2d00d8b1faca2dfa5330db572f6',
    '900' => $fontDir . '/font-awesome-5-free-900_716a505c720f3076ba191932863d46ac',
  ),
) ?>