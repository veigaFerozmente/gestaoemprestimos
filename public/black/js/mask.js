$(document).ready(function(){
    $('#telefone').mask('(99) 9 9999-9999');
    $("#cpf").mask("999.999.999-99");
    $("#dt_vencimento").mask("99/99/9999");
    $('#valor').mask('#.##0,00', {reverse: true});
    $('#vlr_pagar').mask('#.##0,00', {reverse: true});
});
