demo = {
  var ctx = document.getElementById("chartLinePurple").getContext("2d");

  var gradientStroke = ctx.createLinearGradient(0, 230, 0, 50);

  gradientStroke.addColorStop(1, 'rgba(72,72,176,0.2)');
  gradientStroke.addColorStop(0.2, 'rgba(72,72,176,0.0)');
  gradientStroke.addColorStop(0, 'rgba(119,52,169,0)'); //purple colors

  var vendaMediaMensal = $(".total_jun");
  console.log(vendaMediaMensal);
  var data = {
    labels: ["Jan", "Fev", "Mar", "Abr", "Mai", "Jun", "Jul", "Aug", "Set", "Out", "Nov", "Dez"],
    datasets: [{
      label: "Data",
      fill: true,
      backgroundColor: gradientStroke,
      borderColor: '#1877F2',
      borderWidth: 2,
      borderDash: [],
      borderDashOffset: 0.0,
      pointBackgroundColor: '#1877F2',
      pointBorderColor: 'rgba(255,255,255,0)',
      pointHoverBackgroundColor: '#1877F2',
      pointBorderWidth: 20,
      pointHoverRadius: 4,
      pointHoverBorderWidth: 15,
      pointRadius: 4,
      data: [0,0,0,0,vendaMediaMensal],
    }]
  };

  var myChart = new Chart(ctx, {
    type: 'line',
    data: data,
    options: gradientChartOptionsConfigurationWithTooltipPurple
  });
}
