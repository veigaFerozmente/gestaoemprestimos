<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
*/

Route::get('/', 'HomeController@index')->name('home');
Route::get('/home', 'HomeController@index')->name('home');
Auth::routes(['reset' => false, 'register' => false]);


Route::get('/home', 'HomeController@index')->name('home')->middleware('auth');

Route::get('/graficos/graficos-total-emprestimos-mensal', 'GraficosController@graficosEmprestimosTotalMensal')->middleware('auth');
Route::get('/graficos/graficos-total-pagamento-mensais', 'GraficosController@graficosTotalPagamentoMensal')->middleware('auth');

Route::group(['middleware' => 'auth'], function () {
	Route::resource('user', 'UserController', ['except' => ['show']]);
	Route::get('profile', ['as' => 'profile.edit', 'uses' => 'ProfileController@edit']);
	Route::put('profile', ['as' => 'profile.update', 'uses' => 'ProfileController@update']);
	Route::put('profile/password', ['as' => 'profile.password', 'uses' => 'ProfileController@password']);

	Route::get('config', ['as' => 'config.edit', 'uses' => 'ConfiguracaoController@edit']);
	Route::put('config', ['as' => 'config.update', 'uses' => 'ConfiguracaoController@update']);
});

Route::group(['middleware' => 'auth'], function () {
	Route::resource('clientes', 'ClientesController', ['except' => ['show']]);
});


Route::group(['middleware' => 'auth'], function () {
	Route::resource('fluxoSaida', 'FluxoSaidaController');
	Route::get('gerar-pdf-emp/{id}', ['as' => 'fluxoSaida.pdf', 'uses' => 'FluxoSaidaController@gerarPdf']);
	Route::post('pagar', ['as' => 'fluxoSaida.pagamento', 'uses' => 'FluxoSaidaController@pagamento']);
	Route::post('pagar-total', ['as' => 'fluxoSaida.pagamento.total', 'uses' => 'FluxoSaidaController@pagamentoTotal']);
});
