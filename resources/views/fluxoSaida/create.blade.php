@extends('layouts.app', ['page' => __('Novo Emprestimo'), 'pageSlug' => 'emprestimos'])

@section('content')
    <div class="row">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    <h5 class="title">{{ __('Novo Emprestimo') }}</h5>
                </div>
                <form method="post" action="{{ route('fluxoSaida.store') }}" autocomplete="off">
                    <div class="card-body">
                            @csrf
                            @method('post')

                            <div class="form-group{{ $errors->has('cliente') ? ' has-danger' : '' }}">
                                <label>{{ __('Cliente') }}</label>
                                <select style="background-color: #2b3553" name="cliente" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="cliente">
                                  <option value="0">Selecione o Cliente</option>
                                  @foreach($clientes as $cli)
                                        <option value="{{ $cli->id }}">{{ $cli->name }}</option>
                                   @endforeach
                                </select>
                                @include('alerts.feedback', ['field' => 'cliente'])
                            </div>

                            <div class="form-group{{ $errors->has('valor') ? ' has-danger' : '' }}">
                                <label>{{ __('Valor do Emprestimo') }}</label>
                                <input type="text" id="valor" name="valor" class="form-control{{ $errors->has('valors') ? ' is-invalid' : '' }}" placeholder="{{ __('Valor do emprestimo') }}" value="">
                                @include('alerts.feedback', ['field' => 'valor'])
                            </div>

                            <div class="form-group{{ $errors->has('parcelas') ? ' has-danger' : '' }}">
                                <label>{{ __('Parcelas') }}</label>
                                <input type="text" name="parcelas" class="form-control{{ $errors->has('parcelas') ? ' is-invalid' : '' }}" placeholder="{{ __('Quantidades de Parcelas') }}" value="">
                                @include('alerts.feedback', ['field' => 'parcelas'])
                            </div>

                            <div class="form-group{{ $errors->has('vencimento') ? ' has-danger' : '' }}">
                                <label>{{ __('Data da 1º Parcela') }}</label>
                                <input type="text" id="dt_vencimento" name="vencimento" class="form-control{{ $errors->has('vencimento') ? ' is-invalid' : '' }}" placeholder="{{ __('Data da 1º Parcela') }}" value="">
                                @include('alerts.feedback', ['field' => 'vencimento'])
                            </div>

                            <div class="form-group{{ $errors->has('vencimento') ? ' has-danger' : '' }}">
                                <label>{{ __('Juros') }}</label>
                                <input type="text" name="juros" class="form-control{{ $errors->has('juros') ? ' is-invalid' : '' }}" placeholder="{{ __('Juros') }}" value="">
                                @include('alerts.feedback', ['field' => 'juros'])
                            </div>

                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-fill btn-primary">{{ __('Salvar') }}</button>
                    </div>
                </form>
            </div>
        </div>

        </div>
    </div>
@endsection
