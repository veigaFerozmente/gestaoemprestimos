<table border="2">
  <tbody>
    <tr>
      <td>
          ....
      </td>
      <td>
          Nº
          <span>{{ $emprestimoProm->controlePagamentos[0]->parcela}}</span>
          &nbsp;/
          <span><span>{{ $emprestimoProm->parcelas }}</span></span>
          <br>
          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;
          Vencimento:
          <span>{{ $emprestimoProm->controlePagamentos[0]->vencimento->format('d') }}</span>
          &nbsp;de
          <span>Julho</span>
          &nbsp;de
          <span>{{ $emprestimoProm->controlePagamentos[0]->vencimento->format('Y') }}</span>
          <br>
          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp; Valor: ***<span>{{ $emprestimoProm->valor_parcela }}</span>
          ***<br>
          Aos
          <span>{{ $emprestimoProm->controlePagamentos[0]->vencimento->format('d') }}</span>
          &nbsp;dias do mês de
          <span>Julho</span>
          &nbsp;de
          <span>{{ $emprestimoProm->controlePagamentos[0]->vencimento->format('Y') }}</span>
          &nbsp;***<br>
          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
          *** pagarei por esta única via de <span>N O T A&nbsp;&nbsp; P R O M I
              S S Ó R I A</span><br>
          À
          <span>TESTE TESTE</span>
          &nbsp;- CNPJ/CPF:
          <span>745.744.694-04</span>
          <br>
          <br>
          OU À SUA ORDEM A QUANTIA DE: ***
          <span style="text-decoration:underline;">TREZENTOS E NOVENTA REAIS</span>
          &nbsp;***
          <br>
          EM MOEDA CORRENTE NACIONAL.<br>
          <br>
          Pagável em :
          <span>MÃOS</span>
          <br>
          <br>
          Emitente:
          <span>TESTE</span>
          <br>
          <br>
          <span>TESTE</span>
          ,
          <span>8</span>
          &nbsp;de
          <span>Junho</span>
          &nbsp;de
          <span>2020</span>
          <br>
          <br>
          CNPJ/CPF:
          <span>155.946.967-68</span>
          <br>
          <br>
          Endereço:
          <span>TESTE</span>
          <br>
          <br>
          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
          <span>TESTE</span>
          <br>
      </td>
    </tr>
  </tbody>
</table>
