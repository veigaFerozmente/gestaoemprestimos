@extends('layouts.app', ['page' => __('Emprestimos'), 'pageSlug' => 'emprestimos'])

@section('content')

@if ($message = Session::get('success'))
<div class="alert alert-success">
    <p>{{ $message }}</p>
</div>
@endif

<div class="content">
   <div class="row">
      <div class="col-md-12">
         <div class="card ">
            <div class="card-header">
               <div class="row">
                  <div class="col-8">
                     <h4 class="card-title">Emprestimos</h4>
                  </div>
                  <div class="col-4 text-right">
                     <a href="{{ route('fluxoSaida.create') }}" title="Novo empréstimo" class="btn btn-sm btn-dark">
                     <i class="tim-icons icon-money-coins"></i>
                     </a>
                  </div>
               </div>
            </div>
            <div class="card-body">
              @if ($message = Session::get('success'))
              <div class="alert alert-success">
                  <p>{{ $message }}</p>
              </div>
              @endif
            </div>
            <div class="card-body">
               <div class="table-responsive">
                  <table class="table tablesorter" id="">
                     <thead class=" text-primary">
                        <tr>
                           <th scope="col">Cliente</th>
                           <th scope="col">Valor</th>
                           <th scope="col">Parcelas</th>
                           <th scope="col">1° Venc</th>
                           <th scope="col">Juros</th>
                           <th scope="col">Total</th>
                           <th scope="col">Solicitaçao</th>
                           <th scope="col">Status</th>
                           <th scope="col"></th>
                        </tr>
                     </thead>
                     <tbody>
                        @foreach ($emprestimos as $emp)
                        <tr>
                           <td>{{ $emp->clientes->name }}</td>
                           <td>
                              <span style="color:red">
                              R$ {{ number_format($emp->valor,2) }}
                              </span>
                           </td>
                           <td>{{ $emp->parcelas }} / R$ {{ $emp->valor_parcela }}</td>
                           <td>
                              <span style="color:teal">
                              {{ $emp->vencimento->format('d/m/Y') }}
                              </span>
                           </td>
                           <td class="text-center">{{ $emp->juros }}%</td>
                           <td>
                              <span style="color:yellow">
                              R$ {{ $emp->valor_total }}
                              </span>
                           </td>
                           <td>{{ $emp->created_at->format('d/m/Y') }}</td>
                           <td>
                              @if($emp->controlePagamentos->sum('vlr_pago') == $emp->valor_total)
                              Pago
                              @else
                              Pendente
                              @endif
                           </td>
                           <td class="text-right">
                              <div class="dropdown">
                                 <a class="btn btn-sm btn-icon-only text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                 <i class="fas fa-ellipsis-v"></i>
                                 </a>
                                 <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                                    <a class="dropdown-item" href="{{ route('fluxoSaida.show',$emp->id) }}">Informações</a>
                                    <a class="dropdown-item" target="_blank" href="{{ route('fluxoSaida.pdf' ,$emp->id) }}">Gerar PDF</a>
                                    <!-- <a class="dropdown-item" href="{{ route('clientes.edit',$emp->id) }}">Editar</a> -->
                                    <form action="{{ route('fluxoSaida.destroy',$emp->id) }}" method="POST">
                                       @csrf
                                       @method('DELETE')
                                       <button type="submit" class="dropdown-item">Excluir</button>
                                    </form>
                                 </div>
                              </div>
                           </td>
                        </tr>
                        @endforeach
                     </tbody>
                  </table>
                  <div>
                    {{ $emprestimos->links() }}
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
@endsection
