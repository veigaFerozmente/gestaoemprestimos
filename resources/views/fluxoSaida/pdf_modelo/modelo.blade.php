@extends('layouts.app', ['page' => __('Emprestimos'), 'pageSlug' => 'emprestimos'])

<div class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="card ">
        <div class="card-header">
          <div class="row">
              <div class="col-8">
                  <h4 class="card-title">Relatorio</h4>
              </div>
          </div>
          <div class="card-body">
            <div class="table-responsive">
                <table class="table tablesorter " id="">
                    <thead class=" text-primary">
                        <tr>
                        <th scope="col">Cliente</th>
                        <th scope="col">Valor</th>
                        <th scope="col">Parcelas</th>
                        <th scope="col">Juros ao Mês</th>
                        <th scope="col">Dia Venc</th>
                        <th scope="col">Total a pagar</th>
                        <th scope="col">Solicitaçao</th>
                    </tr>
                  </thead>
                    <tbody>
                      <tr>
                        <td>{{ $emprestimo->clientes->name }}</td>
                        <td>R$ {{ $emprestimo->valor }}</td>
                        <td>{{ $emprestimo->parcelas }}</td>
                        <td>{{ $emprestimo->juros }}%</td>
                        <td>{{ $emprestimo->vencimento->format('d') }}</td>
                        <td>R$ {{ $emprestimo->valor_total }}</td>
                        <td>{{ $emprestimo->created_at->format('d/m/Y') }}</td>
                      </tr>
                  </tbody>
                </table>
            </div>
          </div>

          <div class="card-body">
            <div class="table-responsive">
            <table class="table tablesorter">
                <thead class=" text-primary" data-id="{{ $emprestimo->id }}">
                    <tr>
                    <th scope="col">Parc</th>
                    <th scope="col">Datas</th>
                    <th scope="col">Valor</th>
                    <th scope="col">Pago</th>
                    <th scope="col">Saldo</th>
                    <th scope="col">Situaçao</th>
                    <th scope="col"></th>
                    <th scope="col"></th>
                    <th scope="col"></th>
                </tr>
              </thead>
                <tbody>
                  @foreach($emprestimo->controlePagamentos as $key => $contr)
                  <tr>
                    <td>{{ $key+1 }}</td>
                    <td>{{ $contr->vencimento->format('d/m/Y') }}</td>
                    <td>R$ {{ $emprestimo->valor_parcela }}</td>
                    <td>R$ {{ $contr->vlr_pago }}</td>
                    <td>R$ {{ $contr->vlr_saldo }}</td>
                    <td>{{ strtoupper($contr->situacao) }}</td>
                  </tr>
                  @endforeach
              </tbody>
            </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
