@extends('layouts.app', ['page' => __('Gestão de Clientes'), 'pageSlug' => 'emprestimos'])

@section('content')
    <div class="row">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    <h5 class="title">{{ __('Novo Cliente') }}</h5>
                </div>
                <form method="POST" action="{{ route('clientes.update',$cliente->id) }}" autocomplete="off">
                    <div class="card-body">
                            @csrf
                            @method('PUT')

                            <div class="form-group{{ $errors->has('name') ? ' has-danger' : '' }}">
                                <label>{{ __('Nome') }}</label>
                                <input type="text" name="name" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" placeholder="{{ __('Nome') }}" value="{{ $cliente->name }}">
                                @include('alerts.feedback', ['field' => 'name'])
                            </div>

                            <div class="form-group{{ $errors->has('email') ? ' has-danger' : '' }}">
                                <label>{{ __('Email') }}</label>
                                <input type="email" name="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" placeholder="{{ __('E-mail') }}" value="{{ $cliente->email }}">
                                @include('alerts.feedback', ['field' => 'email'])
                            </div>

                            <div class="form-group{{ $errors->has('telefone') ? ' has-danger' : '' }}">
                                <label>{{ __('Telefone') }}</label>
                                <input type="telefone" id="telefone" name="telefone" class="form-control{{ $errors->has('telefone') ? ' is-invalid' : '' }}" placeholder="{{ __('Telefone') }}" value="{{ $cliente->telefone }}">
                                @include('alerts.feedback', ['field' => 'telefone'])
                            </div>

                            <div class="form-group{{ $errors->has('cpf') ? ' has-danger' : '' }}">
                                <label>{{ __('Cpf') }}</label>
                                <input type="cpf" id="cpf" name="cpf" class="form-control{{ $errors->has('cpf') ? ' is-invalid' : '' }}" placeholder="{{ __('cpf') }}" value="{{ $cliente->cpf }}">
                                @include('alerts.feedback', ['field' => 'cpf'])
                            </div>

                            <div class="form-group{{ $errors->has('indicacao') ? ' has-danger' : '' }}">
                                <label>{{ __('Indicação') }}</label>
                                <input type="cpf" name="indicacao" class="form-control{{ $errors->has('indicacao') ? ' is-invalid' : '' }}" placeholder="{{ __('indicacao') }}" value="{{ $cliente->indicacao }}">
                                @include('alerts.feedback', ['field' => 'indicacao'])
                            </div>

                            <div class="form-group{{ $errors->has('status') ? ' has-danger' : '' }}">
                                <div class="btn-group btn-group-toggle" data-toggle="buttons">
                                  <label class="btn btn-light {{ $status['a'] }}">
                                    <input type="radio" name="status" id="ativo" value="A" checked> Ativo
                                  </label>
                                  <label class="btn btn-light {{ $status['d'] }}">
                                    <input type="radio" name="status" id="desativado" value="D"> Desativado
                                  </label>
                                </div>
                            </div>
                    </div>
                    <div class="card-footer">
                        <input type="hidden" name="id" value="{{ $cliente->id }}">
                        <button type="submit" class="btn btn-fill btn-primary">{{ __('Atualizar') }}</button>
                    </div>
                </form>
            </div>
        </div>
        <div class="col-md-4">
            <div class="card card-user">
                <div class="card-body">
                    <p class="card-text">
                        <div class="author">
                            <div class="block block-one"></div>
                            <div class="block block-two"></div>
                            <div class="block block-three"></div>
                            <div class="block block-four"></div>
                            <a href="#">
                                <h4 class="title">Bem vindo a edição de clientes</h4>
                                <h5 class="title">{{ auth()->user()->name }}</h5>
                            </a>
                            <p class="description">
                                {{ __('Teste') }}
                            </p>
                        </div>
                    </p>
                    <div class="card-description">
                        {{ __('Tela destinada ao cadastro de clientes') }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
