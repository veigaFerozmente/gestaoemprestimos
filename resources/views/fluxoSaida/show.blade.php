@extends('layouts.app', ['page' => __('Emprestimos'), 'pageSlug' => 'emprestimos'])

@section('content')

<div id="modalPag" class="modal">
    <form method="post" action="{{ route('fluxoSaida.pagamento') }}" autocomplete="off">
        <div class="card-body">
                @csrf
                @method('post')

                <div class="form-group">
                    <label>{{ __('Valor a Pagar') }}</label>
                    <input type="text" id="vlr_pagar" name="vlr_pago" class="form-control" style="color: #000 !important;" placeholder="{{ __('Valor a Pagar') }}" value="">
                    <input type="hidden" id="id_pagamento" name="id_pagamento">
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-fill btn-primary">{{ __('Pagar') }}</button>
                </div>
        </div>
    </form>
  <!-- <a href="#" rel="modal:close">Close</a> -->
</div>

<div class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="card ">
        <div class="card-header">
          <div class="row">
              <div class="col-8">
                  <h4 class="card-title">Visualizar Emprestimo</h4>
              </div>
              <div class="col-4 text-right">
                  <a href="{{ route('fluxoSaida.index') }}" class="btn btn-sm btn-primary">Voltar</a>
              </div>
          </div>
          <div class="card-body">
            <div class="table-responsive">
                <table class="table tablesorter " id="">
                    <thead class=" text-primary">
                        <tr>
                        <th scope="col">Cliente</th>
                        <th scope="col">Valor</th>
                        <th scope="col">Parcelas</th>
                        <th scope="col">Juros ao Mês</th>
                        <th scope="col">Dia Venc</th>
                        <th scope="col">Total a pagar</th>
                        <th scope="col">Solicitaçao</th>
                    </tr>
                  </thead>
                    <tbody>
                      <tr>
                        <td>{{ $emprestimo->clientes->name }}</td>
                        <td>R$ {{ $emprestimo->valor }}</td>
                        <td>{{ $emprestimo->parcelas }}</td>
                        <td>{{ $emprestimo->juros }}%</td>
                        <td>{{ $emprestimo->vencimento->format('d') }}</td>
                        <td>R$ {{ $emprestimo->valor_total }}</td>
                        <td>{{ $emprestimo->created_at->format('d/m/Y') }}</td>
                        <td>
                          <span><i class="fa fa-whatsapp" style="font-size:25px" title="Enviar Whats"></i></span>
                          <span><i class="fa fa-envelope" style="font-size:25px" title="Enviar Email"></i></span>
                        </td>
                      </tr>
                  </tbody>
                </table>
            </div>
          </div>

          <div class="card-body">
            <div class="table-responsive">
            <table class="table tablesorter">
                <thead class=" text-primary" data-id="{{ $emprestimo->id }}">
                    <tr>
                    <th scope="col">Parc</th>
                    <th scope="col">Datas</th>
                    <th scope="col">Valor</th>
                    <th scope="col">Pago</th>
                    <th scope="col">Saldo</th>
                    <th scope="col">Situaçao</th>
                    <th scope="col"></th>
                    <th scope="col"></th>
                    <th scope="col"></th>
                </tr>
              </thead>
                <tbody>
                  @foreach($emprestimo->controlePagamentos as $key => $contr)
                  <tr>
                    <td>{{ $key+1 }}</td>
                    <td>{{ $contr->vencimento->format('d/m/Y') }}</td>
                    <td>R$ {{ $emprestimo->valor_parcela }}</td>
                    <td>R$ {{ $contr->vlr_pago }}</td>
                    <td>R$ {{ $contr->vlr_saldo }}</td>
                    <td>{{ strtoupper($contr->situacao) }}</td>
                    <td>
                      <a href="..\{{ $contr->promissoria }}" target="_blank">
                        <i title="Gerar Promissória" class="fa fa-file-pdf-o" aria-hidden="true" style="font-size: 25px"></i>
                      </a>
                    </td>
                    <td class="text-center">
                      @if($contr->vlr_saldo > '0.01' || $contr->vlr_pago == '0.00')
                        <form method="post" action="{{ route('fluxoSaida.pagamento.total') }}" autocomplete="off">
                          @csrf
                          @method('post')

                         <input type="hidden" name="id" value="{{$contr->id}}">
                         <input type="hidden" name="saldo" value="{{$contr->vlr_saldo}}">
                         <button type="submit" class="btn btn-sm btn-icon-only text-light">Quitar</button>
                      </form>
                      @endif
                    </td>
                    <td>
                      @if($contr->vlr_saldo > '0.01' || $contr->vlr_pago == '0.00')
                      <a class="btn btn-sm btn-icon-only text-light pagamento" href="#modalPag"  rel="modal:open" role="button" data-id="{{ $contr->id }}">
                        Parcelar
                      </a>
                      @endif
                    </td>
                  </tr>
                  @endforeach
              </tbody>
            </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
