<div class="sidebar">
    <div class="sidebar-wrapper">
        <div class="logo">
            <a href="#" class="simple-text logo-mini">{{ __('WL') }}</a>
            <a href="#" class="simple-text logo-normal">{{ __('Gestão') }}</a>
        </div>
        <ul class="nav">
            <li class="{{ $pageSlug == 'dashboard' ? ' active' : '' }}">
                <a href="{{ route('home') }}">
                    <i class="tim-icons icon-tap-02"></i>
                    <p>{{ __('Home') }}</p>
                </a>
            </li>
<!--             <li>
                <a data-toggle="collapse" href="#laravel-examples" aria-expanded="true">
                    <i class="fab fa-laravel" ></i>
                    <span class="nav-link-text" >{{ __('Usuários') }}</span>
                    <b class="caret mt-1"></b>
                </a>

                <div class="collapse show" id="laravel-examples">
                    <ul class="nav pl-4">
                        <li @if ($pageSlug ?? '' == 'profile') class="active " @endif>
                            <a href="{{ route('profile.edit')  }}">
                                <i class="tim-icons icon-single-02"></i>
                                <p>{{ __('Perfil do Usuário') }}</p>
                            </a>
                        </li>
                        <li @if ($pageSlug ?? '' == 'users') class="active " @endif>
                            <a href="{{ route('user.index')  }}">
                                <i class="tim-icons icon-bullet-list-67"></i>
                                <p>{{ __('Lista de Usuários') }}</p>
                            </a>
                        </li>
                    </ul>
                </div>
            </li> -->

            <li class="{{ $pageSlug == 'clientes' ? ' active' : '' }}">
                <a href="{{ route('clientes.index')  }}">
                  <i class="tim-icons icon-single-02" ></i>
                    <p>{{ __('Clientes') }}</p>
                </a>
            </li>
            <li class="{{ $pageSlug == 'emprestimos' ? ' active' : '' }}">
                <a href="{{ route('fluxoSaida.index')  }}">
                    <i class="tim-icons icon-bank"></i>
                    <p>{{ __('Emprestimos') }}</p>
                </a>
            </li>
            <li class="{{ $pageSlug == 'config' ? ' active' : '' }}">
                <a href="{{ route('config.edit')  }}">
                    <i class="tim-icons icon-settings"></i>
                    <p>{{ __('Configurações') }}</p>
                </a>
            </li>
        </ul>
    </div>
</div>
