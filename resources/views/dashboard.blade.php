@extends('layouts.app', ['pageSlug' => 'dashboard'])

@section('content')
    <div class="row">
        @include('home.total_pago_grafico_anual_box')
        @include('home.box_grafico')
        @include('home.parcelas_vencidas')
        @include('home.parcelas_mes_vencimento')
        <!-- @include('home.saldos') -->
    </div>
@endsection

@push('js')
    <script src="{{ asset('black') }}/js/plugins/chartjs.min.js"></script>
    <script>
        $(document).ready(function() {
          demo.initDashboardPageCharts();
        });
    </script>
@endpush
