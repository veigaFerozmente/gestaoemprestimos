@extends('layouts.app', ['page' => __('Gestão de Clientes'), 'pageSlug' => 'clientes'])

@section('content')
    <div class="row">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    <h5 class="title">{{ __('Novo Cliente') }}</h5>
                </div>
                <form method="POST" action="{{ route('clientes.store') }}" autocomplete="off">
                    <div class="card-body">
                            @csrf
                            @method('POST')

                            <div class="form-group{{ $errors->has('name') ? ' has-danger' : '' }}">
                                <label>{{ __('Nome') }}</label>
                                <input type="text" name="name" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" placeholder="{{ __('Nome') }}" value="">
                                @include('alerts.feedback', ['field' => 'name'])
                            </div>

                            <div class="form-group{{ $errors->has('email') ? ' has-danger' : '' }}">
                                <label>{{ __('Email') }}</label>
                                <input type="text" name="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" placeholder="{{ __('E-mail') }}" value="">
                                @include('alerts.feedback', ['field' => 'email'])
                            </div>

                            <div class="form-group{{ $errors->has('telefone') ? ' has-danger' : '' }}">
                                <label>{{ __('Telefone') }}</label>
                                <input type="text" id="telefone" name="telefone" class="form-control{{ $errors->has('telefone') ? ' is-invalid' : '' }}" placeholder="{{ __('Telefone') }}" value="">
                                @include('alerts.feedback', ['field' => 'telefone'])
                            </div>

                            <div class="form-group{{ $errors->has('cpf') ? ' has-danger' : '' }}">
                                <label>{{ __('Cpf') }}</label>
                                <input type="text" id="cpf" name="cpf" class="form-control{{ $errors->has('cpf') ? ' is-invalid' : '' }}" placeholder="{{ __('cpf') }}" value="">
                                @include('alerts.feedback', ['field' => 'cpf'])
                            </div>

                            <div class="form-group{{ $errors->has('indicacao') ? ' has-danger' : '' }}">
                                <label>{{ __('Indicação') }}</label>
                                <input type="text" name="indicacao" class="form-control{{ $errors->has('indicacao') ? ' is-invalid' : '' }}" placeholder="{{ __('indicacao') }}" value="">
                                @include('alerts.feedback', ['field' => 'indicacao'])
                            </div>

                            <div class="form-group{{ $errors->has('status') ? ' has-danger' : '' }}">
                                <div class="btn-group btn-group-toggle" data-toggle="buttons">
                                  <label class="btn btn-light active">
                                    <input type="radio" name="status" id="ativo" value="A" checked> Ativo
                                  </label>
                                  <label class="btn btn-light">
                                    <input type="radio" name="status" id="desativado" value="D"> Desativado
                                  </label>
                                </div>
                            </div>
                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-fill btn-primary">{{ __('Salvar') }}</button>
                    </div>
                </form>
            </div>
        </div>
        <div class="col-md-4">
            <div class="card card-user">
                <div class="card-body">
                    <p class="card-text">
                        <div class="author">
                            <div class="block block-one"></div>
                            <div class="block block-two"></div>
                            <div class="block block-three"></div>
                            <div class="block block-four"></div>
                            <a href="#">
                                <h4 class="title">Bem vindo ao cadastro de clientes</h4>
                                <h5 class="title">{{ auth()->user()->name }}</h5>
                            </a>
                            <p class="description">
                                {{ __('Teste') }}
                            </p>
                        </div>
                    </p>
                    <div class="card-description">
                        {{ __('Tela destinada ao cadastro de clientes') }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
