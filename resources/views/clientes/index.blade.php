@extends('layouts.app', ['page' => __('Lista de Clientes'), 'pageSlug' => 'clientes'])

@section('content')
<div class="content">
   <div class="row">
      <div class="col-md-12">
         <div class="card ">
            <div class="card-header">
               <div class="row">
                  <div class="col-8">
                     <h4 class="card-title">Clientes</h4>
                  </div>
                  <div class="col-4 text-right">
                     <a href="{{ route('clientes.create') }}" title="Novo Cliente" class="btn btn-sm btn-dark">
                     <i class="tim-icons icon-single-02"></i>
                     </a>
                  </div>
               </div>
            </div>
            <div class="card-body">
               @if ($message = Session::get('success'))
               <div class="alert alert-success">
                  <p>{{ $message }}</p>
               </div>
               @endif
            </div>
            <div class="card-body">
               <div class="table-responsive">
                  <table class="table tablesorter">
                     <thead class=" text-primary">
                        <tr>
                           <th scope="col">Nome</th>
                           <th scope="col">Telefone</th>
                           <th scope="col">CPF</th>
                           <th scope="col">Indicação</th>
                           <th scope="col">Status</th>
                           <th scope="col">Data de Criação</th>
                           <th scope="col"></th>
                        </tr>
                     </thead>
                     <tbody>
                        @foreach ($clientes as $cliente)
                        <tr>
                           <td>{{ $cliente->name }}</td>
                           <td>{{ $cliente->telefone }}</td>
                           <td>{{ $cliente->cpf }}</td>
                           <td>{{ $cliente->indicacao }}</td>
                           <td>
                              @if ($cliente->status =='A')
                              {{ __('Ativo') }}
                              @else
                              {{ __('Desativado') }}
                              @endif
                           </td>
                           <td>{{ $cliente->created_at->format('d/m/Y') }}</td>
                           <td class="text-right">
                              <div class="dropdown">
                                 <a class="btn btn-sm btn-icon-only text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                 <i class="fas fa-ellipsis-v"></i>
                                 </a>
                                 <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                                    <a class="dropdown-item" href="{{ route('clientes.edit',$cliente->id) }}">Editar</a>
                                    <form action="{{ route('clientes.destroy',$cliente->id) }}" method="POST">
                                       @csrf
                                       @method('DELETE')
                                       <button type="submit" class="dropdown-item">Excluir</button>
                                    </form>
                                 </div>
                              </div>
                           </td>
                        </tr>
                        @endforeach
                     </tbody>
                  </table>
                  <div>
                    {{ $clientes->links() }}
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
@endsection
