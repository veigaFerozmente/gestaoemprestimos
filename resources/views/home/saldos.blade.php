<div class="col-lg-8 col-md-12">
    <div class="card card-tasks">
        <div class="card-header ">
            <h6 class="title d-inline">Parcelas em aberto / Saldos</h6>
        </div>
        <div class="card-body">
          <div class="table-tablesorter">
              <table class="table tablesorter" id="">
                <thead class=" text-primary">
                  <tr>
                      <th>
                          Cliente
                      </th>
                      <th>
                        Venc
                      </th>
                      <th>
                          Parc
                      </th>
                      <th class="text-center">
                          Valor Pago
                      </th>
                      <th class="text-center">
                      </th>
                  </tr>
                 </thead>
                 <tbody>
                    @foreach($emprestimosAbertoSaldos as $value)
                    <tr>
                      <td>
                        {{ $value->fluxoSaida->clientes->name}}
                      </td>
                      <td>
                        @if(date('d/m/Y') == $value->vencimento->format('d/m/Y'))
                          <span style="color:red">{{ $value->vencimento->format('d/m/Y') }}</span>
                        @else
                            {{ $value->vencimento->format('d/m/Y') }}
                        @endif
                      </td>
                      <td>
                        {{ $value->parcela }} / {{ $value->fluxoSaida->parcelas }}
                      </td>
                      <td class="text-center">
                       R$ {{ number_format($value->fluxoSaida->valor_parcela,2, ',', '.') }}
                      </td>
                    </tr>
                   @endforeach
                </tbody>
              </table>
            </div>
        </div>
    </div>
</div>
