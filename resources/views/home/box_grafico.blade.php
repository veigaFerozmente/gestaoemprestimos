<div class="col-lg-6">
    <div class="card card-chart">
        <div class="card-header">
            <h6 class="title d-inline">Total de emprestimos realizados em {{ $dataCorrente->format('m/Y') }}</h6>
            <h4 class="card-title">
              {{ $totalEmprestimosMesAtual->total_emprestimos }} emprestimos,
              no total de R$ {{ number_format($totalEmprestimosMesAtual->total_emprestado, 2,',','.')}}
            </h4>
            <h4 class="card-title">
              <!-- @php
                $lucro = $totalEmprestimosMesAtual->valor_corrigido - $totalEmprestimosMesAtual->total_emprestado
              @endphp
              Lucro Estimado
               R$ {{ number_format($lucro, 2,',','.')}} -->
            </h4>
        </div>
        <div class="card-body">
            <div class="chart-area">
                <canvas id="TotalEmprestimosChart"></canvas>
            </div>
        </div>
    </div>
</div>
