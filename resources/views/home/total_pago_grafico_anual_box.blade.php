<div class="col-lg-6">
    <div class="card card-chart">
        <div class="card-header">
            <h6 class="title d-inline">Pagamentos do mês {{ $dataCorrente->format('m/Y') }}</h6>
            <h3 class="card-title">
              <i class="tim-icons icon-money-coins text-primary"></i>
              R$ {{ number_format($totalPagoMes->total, 2,',','.')}}
            </h3>
        </div>
        <div class="card-body">
            <div class="chart-area">
                <canvas id="PagamentosAnualChart"></canvas>
            </div>
        </div>
    </div>
</div>
