<div class="col-lg-6 col-md-12">
    <div class="card card-tasks">
        <div class="card-header ">
            <h6 class="title d-inline">Vencimento de parcelas do mês {{ $dataCorrente->format('m/Y') }}</h6>
        </div>
        <div class="card-body">
          <div class="table-responsive">
          <div class="table-tablesorter">
              <table class="table tablesorter" id="">
                <thead class=" text-primary">
                  <tr>
                      <th>
                          Cliente
                      </th>
                      <th>
                        Venc
                      </th>
                      <th>
                          Parc
                      </th>
                      <th class="text-center">
                          Valor
                      </th>
                      <th></th>
                  </tr>
                 </thead>
                 <tbody>
                    @foreach($emprestimosAVencer as $value)
                    <tr>
                      <td>
                        {{ $value->fluxoSaida->clientes->name}}
                      </td>
                      <td>
                        @if($dataCorrente->format('d/m/Y') == $value->vencimento->format('d/m/Y'))
                          <span style="color:red">{{ $value->vencimento->format('d/m/Y') }}</span>
                        @else
                            {{ $value->vencimento->format('d/m/Y') }}
                        @endif
                      </td>
                      <td>
                        {{ $value->parcela }} / {{ $value->fluxoSaida->parcelas }}
                      </td>
                      <td class="text-center">
                       R$ {{ number_format($value->fluxoSaida->valor_parcela,2, ',', '.') }}
                      </td>
                      <td>
                        <div class="dropdown">
                            <a class="btn btn-sm btn-icon-only text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="fas fa-ellipsis-v"></i>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                              <a class="dropdown-item" target="_blank" href="{{ route('fluxoSaida.show',$value->id_fluxo_saidas) }}">
                                Informações
                              </a>
                            </div>
                        </div>
                      </td>
                    </tr>
                   @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
    </div>
</div>
