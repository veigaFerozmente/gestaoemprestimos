<?php

namespace App\Http\Controllers;
use App\Config;


class ConfiguracaoController extends Controller
{

    public function edit()
    {
        $config = Config::orderBy('created_at', 'desc')->paginate(1);
        return view('configuracao.edit',['config' => $config]);
    }

    public function update($request)
    {
        auth()->user()->update($request->all());

        return back()->withStatus(__('Configurações Atualizadas com Sucesso.'));
    }

}
