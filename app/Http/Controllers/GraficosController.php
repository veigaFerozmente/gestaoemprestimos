<?php

namespace App\Http\Controllers;
use App\FluxoSaida;
use App\ControlePagamentos;
use Carbon\Carbon;
Use DB;

class GraficosController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $dataCorrente = Carbon::now('America/Sao_Paulo');
        $ultimoDiaMesAtual = date("Y-m-t", strtotime($dataCorrente));
        $mesAtual = $dataCorrente->month;
    }

    public function graficosTotalPagamentoMensal()
    {
      DB::statement("SET lc_time_names = 'pt_PT'");
      $graficosTotalMeses = ControlePagamentos::select(
            DB::raw('sum( vlr_pago ) as total,
            MONTHNAME(vencimento) as mes_nome,
            MONTH(vencimento) as mes'
            ) )
        ->where('vlr_pago','>','0')
        ->groupBy('mes','mes_nome')
        ->orderBy('mes','asc')
        ->get();

        return json_encode($graficosTotalMeses);
    }

    public function graficosEmprestimosTotalMensal()
    {
      DB::statement("SET lc_time_names = 'pt_PT'");
      $graficosTotalEmprMeses = FluxoSaida::select(
            DB::raw('COUNT(id) total_emprestimos,
            SUM(valor) as total_emprestado,
            SUM(valor_total) as valor_corrigido,
            MONTHNAME(created_at) as mes_nome,
            MONTH(created_at) as mes'
          ) )
        ->whereYear('created_at','2020')
        ->groupBy('mes','mes_nome')
        ->orderBy('mes','asc')
        ->get();

        return json_encode($graficosTotalEmprMeses);
    }

}
