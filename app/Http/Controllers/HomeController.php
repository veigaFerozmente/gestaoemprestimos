<?php

namespace App\Http\Controllers;
use App\FluxoSaida;
use App\ControlePagamentos;
use Carbon\Carbon;
Use DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $dataCorrente = Carbon::now('America/Sao_Paulo');
        $ultimoDiaMesAtual = date("Y-m-t", strtotime($dataCorrente));
        $mesAtual = $dataCorrente->month;

        $emprestimosVencidos = ControlePagamentos::with('fluxoSaida.controlePagamentos','fluxoSaida.clientes')
          ->where('vencimento','<', $dataCorrente->format('Y-m-d'))
          ->where('situacao','pendente')
          ->get();

        $emprestimosAVencer = ControlePagamentos::with('fluxoSaida.controlePagamentos','fluxoSaida.clientes')
            ->where('vencimento','>=', $dataCorrente->format('Y-m-d'))
            ->where('vencimento','<=', $ultimoDiaMesAtual)
            ->where('situacao','pendente')
            ->get();

        $totalPagoMes = ControlePagamentos::select( DB::raw('sum( vlr_pago ) as total') )
        ->where('vencimento','>=', $dataCorrente->format('Y-m-').'01')
        ->where('vencimento','<=', $ultimoDiaMesAtual)
        ->first();

        $totalEmprestimosMesAtual = FluxoSaida::select(
              DB::raw('COUNT(id) total_emprestimos,
              SUM(valor) as total_emprestado,
              SUM(valor_total) as valor_corrigido'
              ) )
          ->whereMonth('created_at',$mesAtual)
          ->first();

        $emprestimosAbertoSaldos = $emprestimosAVencer;

        return view('dashboard',
          compact('emprestimosVencidos','emprestimosAbertoSaldos',
                  'emprestimosAVencer','dataCorrente',
                  'totalPagoMes','totalEmprestimosMesAtual'
                  )
        );
    }
}
