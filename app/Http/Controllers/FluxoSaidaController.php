<?php

namespace App\Http\Controllers;

use App\FluxoSaida;
use App\Clientes;
use App\ControlePagamentos;
use Carbon\Carbon;
use App\Http\Requests\FluxoSaidaRequest;
use DB;

class FluxoSaidaController extends Controller
{
    public function index()
    {
        $emprestimos = FluxoSaida::with('clientes.fluxoSaida','controlePagamentos.fluxoSaida')
          ->where('status','A')
          ->orderBy('created_at','desc')
          ->paginate(6);

        return view('fluxoSaida.index',['emprestimos' => $emprestimos]);
    }

    /**
     * Novo emprestimo
     *
     * @return \Illuminate\View\View
     */

    public function create()
    {
        $clientes = Clientes::orderBy('created_at', 'desc')->get();
        return view('fluxoSaida.create', ['clientes' => $clientes]);
    }

    /**
     * Formata Valores
     *
     * @param $valor
     * @return $valorFormat
     */
    public function formatarValores($valor)
    {
      $source = array('.', ',');
      $replace = array('', '.');
      $valorFormat = str_replace($source, $replace, $valor);

      return $valorFormat;
    }

    /**
     * Calcular juros
     *
     * @param $valor, $parcelas, $juros
     * @return $valores
     */

    public function calculaJuros($valor, $parcelas, $juros)
    {
        $valorParcela = $valor / $parcelas;
        $calculo = $juros * $valor;
        $valorPercentualParcela = $calculo / 100;
        $totalParcela = $valorParcela + $valorPercentualParcela;
        $totalParcela = number_format($totalParcela,2,'.','');
        $totalPagar = $totalParcela * $parcelas;
        $valores = ['totalPagar' => $totalPagar, 'totalParcela' => $totalParcela];

        return $valores;
    }

    /**
     * Salvar Emprestimo
     *
     * @param  \App\Http\Requests\FluxoSaidaRequest  $request
     * @return \Illuminate\Http\RedirectResponse
     */

    public function store(FluxoSaidaRequest $request)
    {

        // $request->validate([
        //   'cliente' => 'required',
        //   'valor' => 'required',
        //   'parcelas' => 'required',
        //   'juros' => 'required',
        //   'vencimento' => 'required',
        // ]);

        $valorPagar = $this->formatarValores($request->valor);
        $valores = $this->calculaJuros($valorPagar, $request->parcelas, $request->juros);
        $vencimento = Carbon::createFromFormat('d/m/Y', $request->vencimento);

        $emprestimo = new FluxoSaida;
        $emprestimo->id_cliente         = $request->cliente;
        $emprestimo->valor              = $valorPagar;
        $emprestimo->parcelas           = $request->parcelas;
        $emprestimo->vencimento         = $vencimento;
        $emprestimo->juros              = $request->juros;
        $emprestimo->valor_parcela      = $valores['totalParcela'];
        $emprestimo->valor_total        = $valores['totalPagar'];
        $emprestimo->status             = 'A';
        $emprestimo->save();

        //$storagePath = public_path().'/promissorias/';
        // if (!file_exists($storagePath)) {
        //   mkdir($storagePath, 0777, true);
        // }

        for ($i=0; $i < $emprestimo->parcelas ; $i++) {
            $dtEmprestimo[] = Carbon::parse($emprestimo->vencimento)->addMonths($i);
            $dtName[] = Carbon::parse($dtEmprestimo[$i])->format('Y-m-d');
            $nomePromissoria[] = $dtName[$i].'-'.$emprestimo->id.'.pdf';

            $controlePag = new ControlePagamentos;
            $controlePag->id_fluxo_saidas = $emprestimo->id;
            $controlePag->vencimento      = $dtEmprestimo[$i];
            $controlePag->situacao        = 'pendente';
            $controlePag->vlr_pago        = '0.00';
            $controlePag->vlr_saldo       = '0.00';
            $controlePag->promissoria     = $nomePromissoria[$i];
            $controlePag->parcela         = $i+1;
            $controlePag->status          = 'A';
            $controlePag->save();

            $emprestimoProm = FluxoSaida::with('clientes.fluxoSaida','controlePagamentos.fluxoSaida')
              ->where('id',$controlePag->id_fluxo_saidas)
              ->first();

             \PDF::loadView('fluxoSaida.promissoria.modelo', compact('emprestimoProm'))
                      ->save($nomePromissoria[$i]);
        }

        return redirect()->route('fluxoSaida.index')
               ->with('success','Emprestimo cadastrado com sucesso.');
    }

    /**
     * Vizualizar Emprestimo
     *
     * @param $id
     * @return $emprestimo, $controle_pagamentos
     */

    public function show($id)
    {
        $emprestimo = FluxoSaida::with('clientes.fluxoSaida','controlePagamentos.fluxoSaida')
          ->where('id',$id)
          ->first();

        return view('fluxoSaida.show', compact('emprestimo'));
    }

    /**
     * GERAR PDF
     *
     * @param $id
     * @return $emprestimo
     */

    public function gerarPdf($id)
    {

        $emprestimo = FluxoSaida::with('clientes.fluxoSaida','controlePagamentos.fluxoSaida')
          ->where('id',$id)
          ->first();

        return \PDF::loadView('fluxoSaida.pdf_modelo.modelo', compact('emprestimo'))
                   ->stream();

    }

    /**
     * Controle de Pagamentos
     *
     * @param $request
     * @return
     */

    public function pagamento(FluxoSaidaRequest $request)
    {

      $pagParcela = ControlePagamentos::with('fluxoSaida.controlePagamentos')
        ->where('id',$request->id_pagamento)
        ->first();

      $pagamento = ControlePagamentos::findOrFail($request->id_pagamento);
      $valorPagar = $this->formatarValores($request->vlr_pago);

      if($valorPagar == $pagParcela->fluxoSaida->valor_parcela){
        $pagamento->vlr_pago = $valorPagar;
        $pagamento->situacao = "pago";
        $pagamento->update();
      }else{
        $valorPago = $pagParcela->vlr_pago + $valorPagar;
        $vlr_saldo = $pagParcela->fluxoSaida->valor_parcela - $valorPago;

        $pagamento->vlr_pago = $valorPago;
        if($vlr_saldo <= 0){
            $pagamento->situacao = "pago";
        }else{
            $pagamento->situacao = "pendente";
        }

        $pagamento->vlr_saldo = $vlr_saldo;
        $pagamento->update();
      }

      return back();
    }

    /**
     * Pagamento total da parcela
     *
     * @param $request
     * @return
     */

    public function pagamentoTotal(FluxoSaidaRequest $request)
    {

      $pagParcela = ControlePagamentos::with('fluxoSaida.controlePagamentos')
        ->where('id',$request->id)
        ->first();

      $pagamento = ControlePagamentos::findOrFail($request->id);
      $pagamento->vlr_pago = $pagParcela->fluxoSaida->valor_parcela;
      if($request->saldo){
        $pagamento->vlr_saldo = '0.00';
      }
      $pagamento->situacao  = 'pago';
      $pagamento->update();

      return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\FluxoSaida  $fluxoSaida
     * @return \Illuminate\Http\RedirectResponse
     */

    public function destroy(FluxoSaida $fluxoSaida)
    {
        $fluxoSaida->delete();

        return redirect()->route('fluxoSaida.index')
                        ->with('success','Emprestimo deletado com sucesso');
    }
}
