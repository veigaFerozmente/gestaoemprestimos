<?php

namespace App\Http\Controllers;

use App\Clientes;
use App\Http\Requests\ClientesRequest;

class ClientesController extends Controller
{

    public function index(Clientes $model)
    {
        return view('clientes.index', ['clientes' => $model->paginate(15)]);
    }

    /**
     * Novo cliente
     *
     * @return \Illuminate\View\View
     */

    public function create()
    {
        return view('clientes.create');
    }

    /**
     * Salvar clientes
     *
     * @param  \App\Http\Requests\ClientesRequest  $request
     * @return \Illuminate\Http\RedirectResponse
     */

    public function store(ClientesRequest $request)
    {

      $request->validate([
        'name' => 'required',
        'email' => 'required',
        'telefone' => 'required',
        'cpf' => 'required',
        'indicacao' => 'required',
      ]);

      Clientes::create($request->all());

      return redirect()->route('clientes.index')
                ->with('success','Cliente cadastrado com sucesso.');
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Clientes  $product
     * @return \Illuminate\Http\RedirectResponse
     */

    public function edit(Clientes $cliente)
    {

      $status_a  = "";
      $status_d = "";

      if($cliente->status == 'A'){
        $status_a = "checked";
      }else{
        $status_d = "checked";
      }

      $status = array(
        'a'  => $status_a,
        'd'  => $status_d
      );

      return view('clientes.edit',compact('cliente','status'));
    }

    /**
     * Update the profile
     *
     * @param  \App\Http\Requests\ClientesRequest  $request
     * @return \Illuminate\Http\RedirectResponse
     */

    public function update(ClientesRequest $request)
    {
      $request->validate([
        'name' => 'required',
        'email' => 'required',
        'telefone' => 'required',
        'cpf' => 'required',
        'indicacao' => 'required',
      ]);

      Clientes::whereId($request->id)->update($request->except(['_method','_token']));

      return redirect()->route('clientes.index')
                ->with('success','Cliente atualizado com sucesso.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Clientes  $clientes
     * @return \Illuminate\Http\RedirectResponse
     */

    public function destroy(Clientes $cliente)
    {
        $cliente->delete();

        return redirect()->route('clientes.index')
                        ->with('success','Cliente deletado com sucesso');
    }
}
