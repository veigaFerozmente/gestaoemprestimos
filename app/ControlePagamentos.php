<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class ControlePagamentos extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = [
         'id_fluxo_saidas', 'situacao', 'promissoria',  'vlr_pago', 'vlr_saldo', 'status',
    ];

    protected $dates = ['vencimento', 'created_at'];

    public function fluxoSaida()
    {
        return $this->belongsTo('App\FluxoSaida','id_fluxo_saidas','id');
    }

}
