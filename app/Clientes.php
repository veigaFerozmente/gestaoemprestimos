<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class Clientes extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'telefone', 'cpf', 'indicacao', 'status',
    ];

    protected $dates = ['vencimento', 'created_at'];

    public function fluxoSaida()
    {
      return $this->hasMany('App\FluxoSaida','id_cliente','id');
    }

}
