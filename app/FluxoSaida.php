<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use DB;

class FluxoSaida extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = [
         'id_cliente', 'valor', 'parcelas', 'juros','valor_parcela', 'valor_total', 'status',
    ];

    protected $dates = ['vencimento', 'created_at'];


    public function clientes()
    {
        return $this->belongsTo('App\Clientes','id_cliente','id');
    }

    public function controlePagamentos()
    {
        return $this->hasMany('App\ControlePagamentos','id_fluxo_saidas','id');
    }

}
